{
  description = "Calliope's NixOS Configurations";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/f28c957a927b0d23636123850f7ec15bda9aa2f4";
    reliability.url  = "path:/home/calliope/src/gitlab.com/gitlab-com/gl-infra/reliability"; 
  };

  outputs = { self, nixpkgs, reliability }@inputs: rec {
    legacyPackages = nixpkgs.lib.genAttrs [ "x86_64-linux" ] (system:
      import inputs.nixpkgs {
        inherit system;

        # unfree packages
        config.allowUnfree = true;
      }
    );
    nixosConfigurations = {
      # Work laptop
      codeine = nixpkgs.lib.nixosSystem {
        pkgs = legacyPackages.x86_64-linux;
        system  = "x86_64-linux";
        modules = [
          ./hosts/codeine
          reliability.nixosModules.sre
        ];
      };
      # Calliope\ Some local machine, build me from codeine with: 
      # NIX_SSHOPTS="-o StrictHostKeyChecking=no -T" \
      #nixos-rebuild --build-host calliope@192.168.1.228 \
      #--target-host calliope@192.168.1.228 \
      #--use-remote-sudo --flake '.#adrenaline' switch --impure
      adrenaline = nixpkgs.lib.nixosSystem {
        pkgs = legacyPackages.x86_64-linux;
        system = "x86_64-linux";
        modules = [
          ./hosts/adrenaline
          ./common
        ];
      };
    };
  };
}
