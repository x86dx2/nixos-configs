{ config, lib, pkgs, ... }:

{ 
  config = {
    # Calliope\ Very important :)
    networking.hostName = "adrenaline";

    # Calliope\ Use grub here
    boot.loader.efi.canTouchEfiVariables = true;
    boot.loader.efi.efiSysMountPoint = "/boot/efi";

    # Calliope\ Like to dual boot windows, so lets
    # set that up here.
    boot.loader.grub = {
      devices = [ "nodev" ];
      efiSupport = true;
      enable = true;
      version = 2;

      extraEntries = ''
        menuentry "Reboot" {
          reboot
        }
        menuentry "Poweroff" {
          halt
        }
        menuentry "Windows" {
          insmod part_gpt
          insmod fat
          insmod search_fs_uuid
          insmod chain
          search --fs-uuid --set=root 1234-5678
          chainloader /EFI/Microsoft/Boot/bootmgfw.efi            
        }
        '';
      };

    # Calliope\ Spooky!
    security.sudo.extraRules= [
    {  users = [ "calliope" ];
      commands = [
        { command = "ALL" ;
          options= [ "NOPASSWD" "SETENV" ]; 
        }
      ];
    }
  ];

    boot.kernelModules = [ "kvm-amd" "kvm-intel" ];

    # Calliope\ Use NetworkManager
    networking.networkmanager.enable = true;

    networking.extraHosts =
      ''
        192.168.1.67 codeine
      '';

    # Calliope\ Also quite important
    services.openssh.enable = true;
    
    # Calliope\ Use nvidia drivers
    services.xserver.videoDrivers = [ "nvidia" ];
    hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.stable;
    hardware.opengl.enable = true;

    environment.systemPackages = [
      pkgs.gnumake
    ];
  };

  imports = [
      # Calliope\ Include the hardware scan
      ./hardware-configuration.nix

      # Calliope\ Components we'd like to install
      # These are configured to my defaults, so they dont
      # take any arguments.
      ../../common
    ];
}
