{ config, lib, pkgs, ... }:

{ 
  config = {
    # Calliope\ Very important :)
    networking.hostName = "codeine";

    sound.enable = true;
    hardware.pulseaudio.enable = true;

    # Calliope\ Use systemd rather than init
    # Sort out EFI 
    boot.loader.systemd-boot.enable = true;
    boot.loader.efi.canTouchEfiVariables = true;
    boot.loader.efi.efiSysMountPoint = "/boot/efi";

    boot.kernelModules = [ "kvm-amd" "kvm-intel" ];

    # Calliope\ Use a keyfile here
    boot.initrd.secrets = {
      "/crypto_keyfile.bin" = null;
    }; 

    # Calliope\ Set our location so redshift plays nice
    location.provider = "manual";
    location.latitude = 51.4545;
    location.longitude = 2.5879;

    # Calliope\ I dont actually use WiFi, so ive just disabled it.
    # networking.wireless.enable = true;

    # Calliope\ Use NetworkManager
    networking.networkmanager.enable = true;

    # Calliope\ Also quite important
    services.openssh.enable = true;

    # Calliope\ Some times I need this to test teleport locally
    networking.extraHosts =
      ''
        127.0.0.1  teleport.knottos.gitlab
        192.168.1.228 adrenaline
      '';

    # Calliope\ Some defaults that work well for me on my laptop.
    services.xserver.dpi = 180;
    services.xserver.layout = "gb";

    # Calliope\ Use nvidia drivers
    services.xserver.videoDrivers = ["nvidia"];
    
    # Calliope\ Bonus configuration that I also found works well.
    hardware.nvidia.prime = {
      sync.enable = true;
      nvidiaBusId = "PCI:1:0:0";
      intelBusId = "PCI:0:2:0";
    };

    # Calliope\ Enable the docker daemon
    virtualisation.docker.enable = true;

    # Calliope\ Enable libvirtd
    virtualisation.libvirtd.enable = true;

    # Calliope\ Enable passSecretService for org.freedesktop.secrets...
    services.passSecretService.enable = true;

    programs.ssh.knownHosts = {
      "gitlab.com" = {
        hostNames = [ "gitlab.com" ];
        publicKey = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAfuCHKVTjquxvt6CM6tdG4SLp1Btn/nOeHHE5UOzRdf";
      };
    };

    system.stateVersion = builtins.trace "Evaluating system.stateVersion" "22.11";
  };

  imports = [
      # Calliope\ Include the hardware scan
      ./hardware-configuration.nix

      # Calliope\ Components we'd like to install
      # These are configured to my defaults, so they dont
      # take any arguments.
      ../../common
      ../../common/xserver
      ../../common/xserver/xmonad.nix
      ../../common/picom
      ../../common/redshift
      ../../common/fonts.nix
      ../../common/xterm

      # Calliope\ I share a keyboard between codeine and ibuprofen
      ../../common/hardware/miya68.nix

      # Calliope\ Some codeine-specific scripts
      ./scripts

      # Calliope\ Configuration Secrets :)
      /home/calliope/.gitlab/nix/secrets/gitlab.nix
    ];
}
