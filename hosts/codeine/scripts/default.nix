{ config, lib, pkgs, ... }:
# Calliope\ Some adhoc scripts that I like in my path
let
  scripts = pkgs.stdenv.mkDerivation rec {
    pname   = "codeine-scripts";
    version = "1.0.0";
    srcs    = [
      ./h
      ./obs-create-tenant
      ./get-overlapping-blocks
    ];
    unpackPhase = ''
    for s in $srcs; do
      n=$(stripHash $s)
      cp -r $s $n
    done
    '';
    installPhase = ''
      mkdir -p $out/bin/
      cp -r * $out/bin/
    '';
  };
in
{
  config = {
    environment.systemPackages = with pkgs; [
      scripts
    ];
  };
  imports = [];
}
