{ config, pkgs, ... }:

{
  # Calliope\ Some settings I like
  services.redshift = {
    enable = true;
    temperature.day = 3500;
    temperature.night = 3500;
  };
}