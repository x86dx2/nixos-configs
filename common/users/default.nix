# common/users/default.nix

{ config, pkgs, ... }:

{
  users.users.calliope = {
    isNormalUser = true;
    createHome   = true;

    group        = "users";
    home         = "/home/calliope";

    hashedPassword = "$6$iirq/GpNr6IBdewB$2NzMW1w95yGB3.F11fHhe66ZMug7oYs.vMMlj69FD6RY2VjLbGiyxfotFxj2vQZHW9955unpsPuvz.OSUCgsl0";
    extraGroups    = [
      "wheel"
      "docker"
      "libvirtd"
    ];
    shell = pkgs.bash;
  };
}