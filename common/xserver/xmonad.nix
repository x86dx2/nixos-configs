{ config, pkgs, ... }:

{
  services.xserver = {
    # Calliope\ If we've asked for xmonad, enable xserver obviously
    enable = true;

    # Calliope\ Some defaults that I prefer
    displayManager = {
      defaultSession = "xfce+xmonad";
      lightdm.enable = false;
      sddm.enable = true;
    };

    # Calliope\ Pretty standard XMonad setup
    windowManager = {
      xmonad = {
        enable = true;
        enableContribAndExtras = true;
        extraPackages = haskellPackages : [
          haskellPackages.xmonad-contrib
          haskellPackages.xmonad-extras
          haskellPackages.xmonad
        ];
      };
    };
    # Calliope\ Dont use xterm
    desktopManager = {
      xterm = {
        enable = false;
      };

      # Calliope\ Xfce is nice for notifications etc.
      # But make sure to disable the windowmanager, as we'll
      # use xmonad for that.
      xfce = {
        enable = true;
        noDesktop = true;
        enableXfwm = false;
      };
    };
  };
}