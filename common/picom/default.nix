{ config, pkgs, ... }:

{
  # Calliope\ Enable picom, it makes things look nice :)
  services.picom = {
    enable = true;
    activeOpacity = 1.0;
    inactiveOpacity = 0.9;
    backend = "glx";
    opacityRules = [ 
      "75:class_g = 'URxvt' && focused"
      "60:class_g = 'URxvt' && !focused" 
    ];
    shadow = false;
    shadowOpacity = 0.75;
    vSync = true;
  };
}